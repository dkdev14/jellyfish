import React, { PureComponent } from 'react'
import { ReactComponent as Logo } from '@oceanprotocol/art/logo/logo-white.svg'
import styles from './Header.module.scss'

export default class Header extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <header className={styles.appHeader}>
                <Logo onClick={this.props.nextDisplay.bind(this, 'home')} />
                <h3 className={styles.topLinks}>Jellyfish</h3>
            </header>
        )
    }
}
