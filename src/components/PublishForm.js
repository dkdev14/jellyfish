import React, { Component } from 'react'
import * as ethereumAddress from 'ethereum-address'
import { FormErrors } from './FormErrors'
import Button from './Button'
import Form from './Form/Form'
import Input from './Form/Input'
import Correct from './Correct'
import Incorrect from './Incorrect'

export default class PublishForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: localStorage.getItem('title'),
            description: localStorage.getItem('description'),
            dataurl: localStorage.getItem('dataurl'),
            author: localStorage.getItem('author'),
            address: localStorage.getItem('address'),
            copyright: localStorage.getItem('copyright'),
            errors: {
                title: '',
                description: '',
                dataurl: '',
                author: '',
                address: '',
                copyright: '',
            },
            titleValid: false,
            descriptionValid: false,
            dataurlValid: false,
            authorValid: false,
            addressValid: false,
            formValid: true,
            copyrightValid: false,
            inProcess: false,
            did: null,
            showPublishForm: true
        }
    }

    componentDidMount() {
        let lastPublish = localStorage.getItem("lastPublish")
        if (lastPublish && lastPublish.length) {
            this.clearLocalStorage();
            this.setState({
                title: "",
                description: "",
                dataurl: "",
                author: "",
                copyright: "",
            })
        }
    }

    handleUserInput(e) {
        const { name, value } = e.target

        this.setState({ [name]: value }, () => {
            this.validateField(name, value)
            localStorage.setItem(name, value)
        })
    }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.errors
        let {
            titleValid,
            descriptionValid,
            dataurlValid,
            addressValid,
            copyrightValid,
            authorValid
        } = this.state

        switch (fieldName) {
            case 'title':
                titleValid = value.length >= 5
                fieldValidationErrors.title = titleValid ? '' : ' is too short'
                break
            case 'description':
                descriptionValid = value.length >= 6
                fieldValidationErrors.description = descriptionValid
                    ? ''
                    : ' is too short'
                break
            case 'address':
                addressValid = ethereumAddress.isAddress(value)
                fieldValidationErrors.address = addressValid
                    ? ''
                    : ' is not a valid ethereum address'
                break
            case 'dataurl':
                dataurlValid = this.isValidUrl(value)
                fieldValidationErrors.dataurl = dataurlValid
                    ? ''
                    : ' is not a valid url'
                break
            case 'author':
                authorValid = value.length >= 1
                fieldValidationErrors.author = authorValid
                    ? ''
                    : ' is too short'
                break
            case 'copyright':
                copyrightValid = value.length >= 1
                fieldValidationErrors.copyright = copyrightValid
                    ? ''
                    : ' is too short'
                break
            default:
                break
        }
        this.setState(
            {
                errors: fieldValidationErrors,
                titleValid,
                descriptionValid,
                dataurlValid,
                authorValid,
                addressValid,
                copyrightValid
            },
            this.validateForm
        )
    }

    validateForm() {
        this.setState({
            formValid:
                this.state.titleValid &&
                this.state.descriptionValid &&
                this.state.dataurlValid &&
                this.state.authorValid &&
                this.state.addressValid &&
                this.state.copyrightValid
        })
    }

    isValidUrl(str) {
        var pattern = new RegExp(
            '^(https?:\\/\\/)?' + // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
            '(\\#[-a-z\\d_]*)?$',
            'i'
        ) // fragment locator
        return !!pattern.test(str)
    }

    processPublishData() {
        let { title, description, address, author, copyright, dataurl } = this.state;
        return {
            "publisher": "",
            "metadata": {
                "main": {
                    "name": title,
                    "dateCreated": new Date().toISOString().substring(0, 19) + 'Z',
                    "author": author,
                    "license": "Apache 2.0",
                    "price": "0",
                    "files": [
                        {
                            "index": 0,
                            "contentType": "application/file",
                            "checksum": "2bf9d229d110d1976cdf85e9f3256c7f",
                            "checksumType": "MD5",
                            "contentLength": "15357507",
                            "compression": "pdf",
                            "encoding": "UTF-8",
                            "url": dataurl
                        }
                    ],

                    "type": "dataset"
                },
                "additionalInformation": {
                    "publishedBy": address,
                    "checksum": "",
                    "categories": [],
                    "tags": [
                        "jellyfish"
                    ],
                    "description": description,
                    "copyrightHolder": copyright,
                    "workExample": "image path, id, label",
                    "links": [],
                    "inLanguage": "en"
                }
            }
        }
    }

    clearLocalStorage() {
        localStorage.removeItem('title');
        localStorage.removeItem('description');
        localStorage.removeItem('author');
        localStorage.removeItem('dataurl');
        localStorage.removeItem('copyright');
    }

    startAgain(isRetry) {
        //update states
        this.setState({
            title: "",
            description: "",
            dataurl: "",
            author: "",
            copyright: "",
            titleValid: false,
            descriptionValid: false,
            dataurlValid: false,
            authorValid: false,
            addressValid: false,
            formValid: true,
            copyrightValid: false,
            inProcess: false,
            did: null,
            showPublishForm: true
        })
    }

    async publishToOcean(e) {
        e.preventDefault()
        e.stopPropagation()

        this.setState({ inProcess: true, showPublishForm: false })
        let publishData = this.processPublishData();
        console.log(JSON.stringify(publishData));
        try {
            const url = `https://agent.oceanprotocol.com/api/general/publishddo`

            const response = await fetch(url, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(publishData)
            })
            const data = await response.json()

            if (response.status !== 200) {
                localStorage.removeItem("lastPublish")
                this.setState({
                    inProcess: false,
                })
                return
            } else {
                localStorage.setItem("lastPublish", true)
                this.setState({
                    inProcess: false,
                    did: data
                })
            }
        } catch (error) {
            console.log(error.message);
        }
    }

    renderPublishSuccess() {
        return (
            <div>
                <Correct loadComplete={true} />
                <p>{`Publish successful 🎉🎉`}
                    <br />
                    <a
                        target="_blank"
                        href={`https://commons.oceanprotocol.com/asset/did:op:${this.state.did}`}
                    >View in Ocean Commons
                    </a></p>


                <div style={{ textAlign: "center" }}>
                    <Button
                        primary
                        onClick={this.startAgain.bind(this, false)}
                    >
                        Close
                </Button>
                </div>
            </div >
        )
    }

    renderPublishFailure() {
        return (
            <>
                <Incorrect />
                <p style={{ color: "#D06079" }}>Oops! some error occured while publishing</p>
                <Button
                    primary
                    onClick={this.startAgain.bind(this, true)}
                >
                    Try Again
                </Button>
            </>
        )
    }
    renderPublishInProgress() {
        return (
            <div>
                <Correct loadComplete={false} />
                <p>Publishing to Ocean 🦑🦑 ...</p>
            </div>
        )
    }

    renderPublishForm() {
        let {
            title,
            description,
            address,
            dataurl,
            author,
            copyright,
            formValid,
            errors
        } = this.state;

        return (
            <Form
                title="Publish To Ocean Commons"
                description="Please describe below details for public data asset."
            >
                <Input
                    type="text"
                    name="title"
                    label="Title"
                    placeholder="Weather of Mumbai"
                    value={title}
                    required
                    help="Enter the title of your dataset"
                    onChange={this.handleUserInput.bind(this)}
                />

                <Input
                    type="text"
                    name="description"
                    label="Description"
                    placeholder="Collection of annual weather report of Mumbai for the year 2019."
                    value={description}
                    required
                    help="Enter the short description of this data asset"
                    onChange={this.handleUserInput.bind(this)}
                />

                <Input
                    type="text"
                    name="address"
                    label="Your Ethereum Address"
                    placeholder="0x3AB766F6ED3345Fa18F50902bD7d660171553421"
                    value={address}
                    required
                    help="Enter your ethereum address to add you as publisher of this asset"
                    onChange={this.handleUserInput.bind(this)}
                />

                <Input
                    type="text"
                    name="dataurl"
                    label="Data Url"
                    placeholder="https://example.com/publicdata.csv"
                    value={dataurl}
                    required
                    help="Enter url where data can be downloaded from"
                    onChange={this.handleUserInput.bind(this)}
                />

                <Input
                    type="text"
                    name="author"
                    label="Author"
                    placeholder="Satoshi Nakamoto"
                    value={author}
                    required
                    help="Who is the author of this data asset?"
                    onChange={this.handleUserInput.bind(this)}
                />
                <Input
                    type="text"
                    name="copyright"
                    label="Copyright"
                    placeholder="Data Commons Foundation"
                    value={copyright}
                    required
                    help="Who owns copyright of this data asset?"
                    onChange={this.handleUserInput.bind(this)}
                />
                <div style={{ textAlign: "center" }}>
                    <Button
                        primary
                        type="submit"
                        disabled={false}
                        onClick={this.publishToOcean.bind(this)}
                    >
                        Publish
                    </Button>
                </div>

                <FormErrors formErrors={errors} />
            </Form>
        )
    }

    render() {

        return (
            <>

                {
                    this.state.showPublishForm ?
                        this.renderPublishForm() :
                        (
                            //is publish in progress
                            this.state.inProcess ?
                                this.renderPublishInProgress() :
                                (
                                    //is asset published
                                    this.state.did ?
                                        // show success
                                        this.renderPublishSuccess() :
                                        //show error
                                        this.renderPublishFailure()
                                )

                        )
                }
            </>
        )
    }
}
