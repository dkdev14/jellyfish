# JellyFish

#### Cross browser extension to publish, search and consume public data assets of Ocean Protocol Commons

> Please note - Jellyfish uses Ocean Protocol's agent (REST API) to publish and consume assets. And it seems agent is down. So, publishing might not work currently. But, you can still search assets because it uses Aquarius.

## Demo Video (Video also shows tutorials on installing Jellyfish on various browsers)
[Youtube link](https://www.youtube.com/watch?v=5ZY552suZQg)

## Development

```bash
# Install dependencies
npm i

# Start development server on browser
npm start
```
## Build Firefox extension
1. ```npm run firefox```
1. After that, follow [these guidelines](https://extensionworkshop.com/documentation/develop/temporary-installation-in-firefox/).
> **Note** - If you get errors while loading extension through browser, try `web-ext` tool. You will run this script in the `build/` folder


## Build Chrome extension
1. ```npm run chrome```
1. load `build/` folder into Chrome extensions (chrome://extensions) page. Here's a small video demo'ing how to do it - [Load Unpacked Browser Extension](https://www.youtube.com/watch?v=oswjtLwCUqg)

## Build Brave Browser extension
1. ```npm run brave```
1. load `build/` folder into Brave extensions (brave://extensions) page. Here's a small video demo'ing how to do it - [Load Unpacked Browser Extension](https://www.youtube.com/watch?v=oswjtLwCUqg)